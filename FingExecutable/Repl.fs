﻿#if INTERACTIVE
fsi.ShowDeclarationValues <- false
#I @"D:\fing\Fing\bin\Debug"
#r "Fing"
#else
module Repl
#endif

open Fing

(*
do
    let argmap = Map.empty<string, seq<string option>>
    //let argmap = argmap.Add("r", [Some @"D:\fing\Fing\bin\Debug\FParsec.dll"])
    let argmap = argmap.Add("r", [Some @"D:\fing\Fing\bin\Debug\FParsec.dll"])
    let getrefs s = Seq.choose id (Opt.mapGet s argmap Seq.empty)
    let references = (getrefs "r" |>Seq.append<| getrefs "reference")

    Fing.addReferences references
    Fing.textSearch "char -> bool"
    Fing.nameFind "TryParse"
*)