﻿// Copyright (c) 2010, Nathan Sanders
// Licence: New BSD. See accompanying documentation.

module Fing
open Microsoft.FSharp.Metadata
open Types
open Util
open Search

[<CustomEquality;CustomComparison>]
type Result = {
    name: string
    ent : FSharpEntity
    mem : FSharpMemberOrVal
    typ : Typ
    hash: int
}
with
    override x.ToString () = x.name
    static member New(ent: FSharpEntity, mem: FSharpMemberOrVal, typ) =
        let name = ent.Namespace + "." + ent.LogicalName + "." + mem.DisplayName
        let hash = name.GetHashCode()
        {
            name = name
            ent = ent
            mem = mem
            typ = typ
            hash = hash
        }

    override x.Equals (yobj) =
        match yobj with
        | :? Result as y ->
            x.hash = y.hash && x.ent = y.ent && x.mem = y.mem && x.typ = y.typ
        | _ -> false

    override x.GetHashCode() = x.hash

    interface System.IComparable with
        member x.CompareTo yobj =
            match yobj with
            | :? Result as y ->
                compare x.hash y.hash
            | _ -> invalidArg "yobj" "cannot compare values of different types"

let entite { ent = e } = e
let membre { mem = m } = m
let tipe { typ = t } = t

let private formatResult { ent = e; mem = m; typ = t } = 
  sprintf "%s.%s\t\t%s" e.DisplayName m.DisplayName (format t)
// TODO: Cache this on disk or something
let mutable private assemblies : string [] = Array.empty

let mutable private types : Set<Result> = Set.empty

let private updateReferences (refs : seq<FSharpAssembly>) =
    types <- 
        Seq.append (Seq.singleton FSharpAssembly.FSharpLibrary) refs
        |> Seq.fold (fun state ref ->
            printfn "Will now do assembly %s" ref.QualifiedName
            let doEntity (e: FSharpEntity) =
                try
                    e.MembersOrValues
                    |> Seq.choose (fun m ->
                        try
                            Some <|
                                Result.New (e, m, 
                                    FSharpTypes.cvt m.Type 
                                    |> index 
                                    |> FSharpTypes.debinarize
                                )
                        with
                        | _ -> None
                    )
                    |> Set.ofSeq
                with
                | _ -> 
                    printfn "Skipping %s because it's crashing metadata" e.DisplayName
                    Set.empty

            let newResults = 
                ref.Entities
                |> Seq.map (fun e ->
                    let rec loop (state: Set<Result>) (es: FSharpEntity list) =
                        match es with
                        | e :: es ->
                            let state = Set.union state (doEntity e)
                            let nestedEntities = 
                                e.NestedEntities
                                |> List.ofSeq
                                |> loop Set.empty
                            let state = Set.union state nestedEntities
                            loop state es
                        | [] ->
                            state
                    loop Set.empty [e]
                )
            let state =
                newResults
                |> Seq.fold (fun state newResult ->
                    newResult
                    |> Set.fold (fun state result ->
                        Set.add result state
                    ) state
                ) state
            printfn "State now has %d elements" state.Count
            state
        ) Set.empty

let addReferences news =
    assemblies <-
        Array.append assemblies news
        |> Seq.distinct
        |> Array.ofSeq
    let optionAssembly assembly =
        try
            FSharpAssembly.FromFile assembly |> Some
        with
        | :? System.IO.FileNotFoundException -> 
            printfn "%s was not found" assembly
            None
        // Indicates a C# assembly, someday I'll handle this
        | :? System.ArgumentException -> 
            printfn "%s is not an F# assembly" assembly
            None
        | _ ->
            printfn "Whatever"
            None
    updateReferences (Seq.choose optionAssembly assemblies)

do addReferences [||]
// Public interface
// (other functions aren't private yet because it's so inconvenient;
// probably I should just move everything else to another module.)
let typeFind s =
    let ty = Parser.parse s |> index |> ParsedTypes.dealias
    types |> Seq.filter (tipe >> matches ty)

let nameFind s = 
    types |> Seq.filter (fun {mem = m} -> m.DisplayName = s)

let search (s : string) =
    if s.Contains "->" then typeFind s else nameFind s

let textSearch s =
    printfn "%s" s
    printfn "Results:"
    Seq.iter (formatResult >> printfn "\t%s") (search s)

let returnTextSearch s =
    search s
    |> Seq.map formatResult

let debug t t' = matches t t'

let getAssemblies () = assemblies