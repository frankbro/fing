namespace Fing.Web

open Fing

open WebSharper.Html.Server
open WebSharper
open WebSharper.Sitelets

type Action =
    | Home

module Skin =
    open System.Web

    type Page =
        {
            Title : string
            Body : list<Element>
        }

    let MainTemplate =
        Content.Template<Page>("~/Main.html")
            .With("title", fun x -> x.Title)
            .With("body", fun x -> x.Body)

    let WithTemplate title body : Content<Action> =
        Content.WithTemplate MainTemplate <| fun context ->
            {
                Title = title
                Body = body context
            }

module Site =

    let arrayElementAsList (displayFun: 't -> Element) (xs: 't []) =
        xs
        |> Array.map (fun x -> LI [ Div [ displayFun x ] ] )
        |> UL

    let HomePage =
        Skin.WithTemplate "Fing" <| fun ctx ->
            [
                yield Div [ClientSide <@ Client.Main() @>]
                yield Br []
                yield Text "Assemblies loaded:"
                yield
                    Fing.getAssemblies ()
                    |> arrayElementAsList (Text)
            ]

    let MainSitelet =
        Sitelet.Sum [
            Sitelet.Content "/" Home HomePage
        ]

module SelfHostedServer =

    open global.Owin
    open Microsoft.Owin.Hosting
    open Microsoft.Owin.StaticFiles
    open Microsoft.Owin.FileSystems
    open WebSharper.Owin

    [<EntryPoint>]
    let Main args = 
        Fing.loadRefs ()
        match args with
        | [| rootDirectory; url |] ->
            use server = WebApp.Start(url, fun (appB: IAppBuilder) ->
                appB.UseStaticFiles(
                        StaticFileOptions(
                            FileSystem = PhysicalFileSystem(rootDirectory)))
                    .UseSitelet(rootDirectory, Site.MainSitelet)
                |> ignore)
            stdout.WriteLine("Serving {0}", url)
            stdin.ReadLine() |> ignore
            0
        | _ ->
            eprintfn "Usage: Fing ROOT_DIRECTORY URL"
            1
