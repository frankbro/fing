namespace Fing.Web

open WebSharper
open WebSharper.JavaScript
open WebSharper.Html.Client

[<JavaScript>]
module Client =

    let Start input k =
        async {
            let! data = Remoting.Process(input)
            return k data
        }
        |> Async.Start

    let Main () =
        let input = Input [Attr.Value ""]
        let label = UL []
        let doit () =
            Start input.Value (fun results -> 
                label.Clear ()
                results
                |> Array.map (fun result -> LI [ Text result ])
                |> Array.iter label.Append
            )
        Div [
            input
            |>! OnKeyDown (fun _ key ->
                match key.KeyCode with
                | 13 ->
                    doit ()
                | _ -> ()
            )
            label
            Button [Text "Click"]
            |>! OnClick (fun _ _ ->
                doit ()
            )
        ]