namespace Fing.Web

open Fing

open WebSharper

module Remoting =

    [<Remote>]
    let Process input =
        async {
            let result = 
                Fing.returnTextSearch input
                |> Array.ofSeq
            return result
        }
